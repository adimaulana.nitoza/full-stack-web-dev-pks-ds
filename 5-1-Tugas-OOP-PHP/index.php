<?php

class Hewan {
    protected $nama;
    protected $darah = 50;
    protected $jumlahKaki;
    protected $keahlian;

    public function setNama($nama){
        $this->nama = $nama;
    }
    public function setDarah($darah){
        $this->darah = $darah;
    }
    public function setJumlahKaki($jumlahKaki){
        $this->jumlahKaki = $jumlahKaki;
    }
    public function setKeahlian($keahlian){
        $this->keahlian = $keahlian;
    }

    public function getNama(){
        echo "Nama : " . $this->nama;
    }
    public function getDarah(){
        echo "Darah : " . $this->darah;
    }
    public function getJumlahKaki(){
        echo "Jumlah Kaki : " . $this->jumlahKaki;
    }
    public function getKeahlian(){
        echo "Keahlian : " . $this->keahlian;
    }

    public function atraksi(){
        echo $this->nama ." " . $this->keahlian;
    }
    public function darah(){
        $this->darah;
    }
    public function getHewan(){
        echo $this->nama;
    }

    public function getInfoHewan(){
        echo "Nama : " . $this->nama . "<br>";
        echo "Darah : " . $this->darah . "<br>";
        echo "Jumlah Kaki : " . $this->jumlahKaki . "<br>";
        echo "Keahlian : " . $this->keahlian . "<br>";
    }
}

class Fight extends Hewan {
    public $attackPower;
    public $defencePower;

    public function setAttackPower($attackPower){
        $this->attackPower = $attackPower;
    }
    public function setDefencePower($defencePower){
        $this->defencePower = $defencePower;
    }

    public function getAttackPower(){
        echo "Atteck Power : " . $this->attackPower;
    }
    public function getDefencePower(){
        echo "Defence Power : " . $this->defencePower;
    }
    

}

class Harimau extends Hewan{
}
class Elang extends Hewan{
}


$harimau = new Harimau;
$harimau->setNama("Harimau_1");
$harimau->setDarah(35);
$harimau->setJumlahKaki(4);
$harimau->setKeahlian("Melompat tinggi");

$elang = new Elang;
$elang->setNama("Elang_1");
$elang->setDarah(40);
$elang->setJumlahKaki(2);
$elang->setKeahlian("Terbang tinggi");



$harimau->getInfoHewan();
echo "<br>";
$elang->getInfoHewan();
echo "<br>";
echo "<br>";
echo "Atraksi Hewan<br>";
$harimau->atraksi();
echo "<br>";
$elang->atraksi();

echo "<br>";
echo "<br>";

echo "<br>---------------<br>";
echo $harimau->getHewan() ." menyerang ";
echo $elang->getHewan();
echo "<br>---------------<br>";

echo "<br>---------------<br>";
echo $elang->getHewan() ." menyerang ";
echo $harimau->getHewan();
echo "<br>---------------<br>";


















?>